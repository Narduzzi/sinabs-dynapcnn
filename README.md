sinabs-dynapcnn
===============

[![codecov](https://codecov.io/gl/synsense/sinabs-dynapcnn/branch/master/graph/badge.svg?token=TJJJBJTHWK)](https://codecov.io/gl/synsense/sinabs-dynapcnn)

This `sinabs` plugin enables porting sinabs models to chips and dev-kits with DYNAP-CNN technology.

Documentation
-------------

You can find the latest documentation at [https://synsense.gitlab.io/sinabs-dynapcnn/](https://synsense.gitlab.io/sinabs-dynapcnn/)

Contact us
----------

- If you would like to report bugs or push any changes, you can do this on out [gitlab repository](https://gitlab.com/synsense/sinabs-dynapcnn).
- We encourage you to join our [SynSense Discord community](https://discordapp.com/channels/852094154188259338/852094154188259342/852113196201934859) to get in touch with the maintainers and other users.
