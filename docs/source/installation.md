Installation
============

Requirements
------------
You will first need to install the latest version of `samna` to be able to install this package.
You can do this by running the following command.

```shell
pip install samna
```

Or if you are installing this package from source repository, you can run:

```shell
pip install -r requirements.txt
```

Installing `sinabs-dynapcnn`
----------------------------

Once the requirements are installed, you can install `sinabs-dynapcnn` using pip.

```shell
pip install sinabs-dynapcnn
```

If you are installing this package from source, you can install it as follows.

```shell
pip install .
```

> Note that you may NOT use `python setup.py install` 
>, or a development install like `pip install . -e`
> as this is a name space package and can be only installed with `pip`.


